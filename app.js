const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3001


const studentRouter = require('./src/router/studentRouter');

app.use(cors({
    credentials: true
}))

app.use('/student', studentRouter);

app.listen(PORT, () => console.log('Student API is running in http://localhost:3001'));


module.exports = app;