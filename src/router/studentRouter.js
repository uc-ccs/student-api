const express = require('express');
const studentRouter = express.Router();
const studentController = require('../controller/studentController')

studentRouter.get('/',studentController.fetchAllStudent);
studentRouter.post('/', studentController.insertStudent);
studentRouter.put('/', studentController.updateStudent);
studentRouter.delete('/:id', studentController.deleteStudent);

module.exports = studentRouter;