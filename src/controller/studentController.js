const studentModel = require('../model/studentModel')


const studentController = {
    fetchAllStudent: async (req, res) => {
        try {
            const result = await studentModel.queryAll();
            return res.status(200).send({message: 'OK', data: result});
        } catch (error) {
            console.log("%c Line:10 🍊 error", "color:#ed9ec7", error);
        }
    },
    insertStudent: async (req, res) => {
        const { body } = req;
        try {
            const result = await studentModel.insert(body)      
            return res.status(200).send({message: 'Ok', data: result})      
        } catch (error) {
            console.log("%c Line:17 🍔 error", "color:#465975", error);
        }
    },
    updateStudent: async (req, res) => {
        const { body } = req;
        try {
            const result = await studentModel.update(body.id, body);
            return res.status(200).send({message: 'Ok', data: result})            
        } catch (error) {
            console.log("%c Line:27 🍣 error", "color:#ea7e5c", error);
        }
    },
    deleteStudent: async (req, res) => {
        const { id } = req.param;
        try {
            const result = await studentModel.delete(id);
            return res.status(200).send({message: 'Ok', data: result})
        } catch (error) {
            console.log("%c Line:36 🥕 error", "color:#6ec1c2", error);
        }
    }
}

module.exports = studentController;