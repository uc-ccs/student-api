var DataStore = require('nedb');
const db = new DataStore({filename: '../db/student.db', autoload: true});


const studentModel = {
    queryAll: () => {
        try {
            db.find({}, (err, docs) => {
                if(err) throw new Error(err);
                return docs;
            })
        } catch (error) {
            console.log(error);
        }
    },
    insert: (payload) => {
        try {
            db.insert(payload, (err, docs) => {
                if(err) throw new Error(err);
                return docs;
            })
        } catch(error){
            console.log(error);
        }
    },
    update: (id, payload) => {
        try {
            db.update({'_id': id},{$set: payload})
        }catch(error) {
            console.log(error)
        }
    },
    delete: (id) => {
        try {
            db.delete({'_id:': id}, {}, (err, numRemoved)=> {
                if(err) throw new Error(err)
                return numRemoved;
            })
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = studentModel;